//tests Projet

//https://mochajs.org/#asynchronous-code

//rank, character, quote
suite("Tests for function sort",
    function () {

        test("Sort by rank",
            function () {
                const state = new State();
                const data = [
                    {"id": "1", "character": "Ralph", "quote": "They taste like burning", "rank": 3},
                    {"id": "2", "character": "Numérobis", "quote": "3 mois? et avec combien de retard?", "rank": 4},
                    {"id": "3", "character": "Iron Man", "quote": "And I'm Iron Man", "rank": 1},
                    {"id": "4", "character": "Vador", "quote": "I'm your father", "rank": 2}
                ];
                const field = "rank";
                const resultat_attendu = [
                    {"id": "3", "character": "Iron Man", "quote": "And I'm Iron Man", "rank": 1},
                    {"id": "4", "character": "Vador", "quote": "I'm your father", "rank": 2},
                    {"id": "1", "character": "Ralph", "quote": "They taste like burning", "rank": 3},
                    {"id": "2", "character": "Numérobis", "quote": "3 mois? et avec combien de retard?", "rank": 4}
                ];
                chai.assert.deepEqual(sort(state, data, field), resultat_attendu);
            });

        test("Sort by rank DESC",
            function () {
                const state = new State();
                state.sorting.rank = false;
                const data = [
                    {"id": "1", "character": "Ralph", "quote": "They taste like burning", "rank": 3},
                    {"id": "2", "character": "Numérobis", "quote": "3 mois? et avec combien de retard?", "rank": 4},
                    {"id": "3", "character": "Iron Man", "quote": "And I'm Iron Man", "rank": 1},
                    {"id": "4", "character": "Vador", "quote": "I'm your father", "rank": 2}
                ];
                const field = "rank";
                const resultat_attendu = [
                    {"id": "2", "character": "Numérobis", "quote": "3 mois? et avec combien de retard?", "rank": 4},
                    {"id": "1", "character": "Ralph", "quote": "They taste like burning", "rank": 3},
                    {"id": "4", "character": "Vador", "quote": "I'm your father", "rank": 2},
                    {"id": "3", "character": "Iron Man", "quote": "And I'm Iron Man", "rank": 1}
                ];

                chai.assert.deepEqual(sort(state, data, field), resultat_attendu);
            });

        test("Sort by character",
            function () {
                const state = new State();
                const data = [
                    {"id": "1", "character": "Ralph", "quote": "They taste like burning", "rank": 2},
                    {"id": "2", "character": "Numérobis", "quote": "3 mois? et avec combien de retard?", "rank": 1},
                    {"id": "3", "character": "Iron Man", "quote": "And I'm Iron Man", "rank": 4},
                    {"id": "4", "character": "Vador", "quote": "I'm your father", "rank": 3}
                ];
                const field = "character";
                const resultat_attendu = [
                    {"id": "3", "character": "Iron Man", "quote": "And I'm Iron Man", "rank": 4},
                    {"id": "2", "character": "Numérobis", "quote": "3 mois? et avec combien de retard?", "rank": 1},
                    {"id": "1", "character": "Ralph", "quote": "They taste like burning", "rank": 2},
                    {"id": "4", "character": "Vador", "quote": "I'm your father", "rank": 3},
                ];
                chai.assert.deepEqual(sort(state, data, field), resultat_attendu);
            });

        test("Sort by character DESC",
            function () {
                const state = new State();
                state.sorting.character = false;
                const data = [
                    {"id": "1", "character": "Ralph", "quote": "They taste like burning", "rank": 2},
                    {"id": "2", "character": "Numérobis", "quote": "3 mois? et avec combien de retard?", "rank": 1},
                    {"id": "3", "character": "Iron Man", "quote": "And I'm Iron Man", "rank": 4},
                    {"id": "4", "character": "Vador", "quote": "I'm your father", "rank": 3}
                ];
                const field = "character";
                const resultat_attendu = [
                    {"id": "4", "character": "Vador", "quote": "I'm your father", "rank": 3},
                    {"id": "1", "character": "Ralph", "quote": "They taste like burning", "rank": 2},
                    {"id": "2", "character": "Numérobis", "quote": "3 mois? et avec combien de retard?", "rank": 1},
                    {"id": "3", "character": "Iron Man", "quote": "And I'm Iron Man", "rank": 4}
                ];
                chai.assert.deepEqual(sort(state, data, field), resultat_attendu);
            });

        test("Sort by quote",
            function () {
                const state = new State();
                const data = [
                    {"id": "1", "character": "Ralph", "quote": "They taste like burning", "rank": 2},
                    {"id": "2", "character": "Numérobis", "quote": "3 mois? et avec combien de retard?", "rank": 1},
                    {"id": "3", "character": "Iron Man", "quote": "And I'm Iron Man", "rank": 4},
                    {"id": "4", "character": "Vador", "quote": "I'm your father", "rank": 3}
                ];
                const field = "quote";
                const resultat_attendu = [
                    {"id": "2", "character": "Numérobis", "quote": "3 mois? et avec combien de retard?", "rank": 1},
                    {"id": "3", "character": "Iron Man", "quote": "And I'm Iron Man", "rank": 4},
                    {"id": "4", "character": "Vador", "quote": "I'm your father", "rank": 3},
                    {"id": "1", "character": "Ralph", "quote": "They taste like burning", "rank": 2}
                ];
                chai.assert.deepEqual(sort(state, data, field), resultat_attendu);
            });

        test("Sort by quote DESC",
            function () {
                const state = new State();
                state.sorting.quote = false;
                const data = [
                    {"id": "1", "character": "Ralph", "quote": "They taste like burning", "rank": 2},
                    {"id": "2", "character": "Numérobis", "quote": "3 mois? et avec combien de retard?", "rank": 1},
                    {"id": "3", "character": "Iron Man", "quote": "And I'm Iron Man", "rank": 4},
                    {"id": "4", "character": "Vador", "quote": "I'm your father", "rank": 3}
                ];
                const field = "quote";
                const resultat_attendu = [
                    {"id": "1", "character": "Ralph", "quote": "They taste like burning", "rank": 2},
                    {"id": "4", "character": "Vador", "quote": "I'm your father", "rank": 3},
                    {"id": "3", "character": "Iron Man", "quote": "And I'm Iron Man", "rank": 4},
                    {"id": "2", "character": "Numérobis", "quote": "3 mois? et avec combien de retard?", "rank": 1}
                ];
                chai.assert.deepEqual(sort(state, data, field), resultat_attendu);
            });

    });

//character / quote
suite("Tests for function filter",
    function () {

        test("Filter by character",
            function () {
                const filterSequence = "on";
                const data = [
                    {"id": "1", "character": "Ralph", "quote": "They taste like burning", "rank": 2},
                    {"id": "2", "character": "Numérobis", "quote": "3 mois? et avec combien de retard?", "rank": 1},
                    {"id": "3", "character": "Iron Man", "quote": "And I'm Iron Man", "rank": 4},
                    {"id": "4", "character": "Vador", "quote": "I'm your father", "rank": 3}
                ];
                const field = "character";
                const resultat_attendu = [
                    {"id": "3", "character": "Iron Man", "quote": "And I'm Iron Man", "rank": 4}
                ];
                chai.assert.deepEqual(filter(filterSequence, data, field), resultat_attendu);
            });

        test("Filter by character no match",
            function () {
                const filterSequence = "zdezfg";
                const data = [
                    {"id": "1", "character": "Ralph", "quote": "They taste like burning", "rank": 2},
                    {"id": "2", "character": "Numérobis", "quote": "3 mois? et avec combien de retard?", "rank": 1},
                    {"id": "3", "character": "Iron Man", "quote": "And I'm Iron Man", "rank": 4},
                    {"id": "4", "character": "Vador", "quote": "I'm your father", "rank": 3}
                ];
                const field = "character";
                const resultat_attendu = [];
                chai.assert.deepEqual(filter(filterSequence, data, field), resultat_attendu);
            });

        test("Filter by quote",
            function () {
                const filterSequence = "rni";
                const data = [
                    {"id": "1", "character": "Ralph", "quote": "They taste like burning", "rank": 2},
                    {"id": "2", "character": "Numérobis", "quote": "3 mois? et avec combien de retard?", "rank": 1},
                    {"id": "3", "character": "Iron Man", "quote": "And I'm Iron Man", "rank": 4},
                    {"id": "4", "character": "Vador", "quote": "I'm your father", "rank": 3}
                ];
                const field = "quote";
                const resultat_attendu = [
                    {"id": "1", "character": "Ralph", "quote": "They taste like burning", "rank": 2}
                ];
                chai.assert.deepEqual(filter(filterSequence, data, field), resultat_attendu);
            });

        test("Filter by quote no match",
            function () {
                const filterSequence = "adzfegr";
                const data = [
                    {"id": "1", "character": "Ralph", "quote": "They taste like burning", "rank": 2},
                    {"id": "2", "character": "Numérobis", "quote": "3 mois? et avec combien de retard?", "rank": 1},
                    {"id": "3", "character": "Iron Man", "quote": "And I'm Iron Man", "rank": 4},
                    {"id": "4", "character": "Vador", "quote": "I'm your father", "rank": 3}
                ];
                const field = "quote";
                const resultat_attendu = [];
                chai.assert.deepEqual(filter(filterSequence, data, field), resultat_attendu);
            });

        test("Filter by quote + filter by character",
            function () {
                const filterSequence1 = "I'm";
                const filterSequence2 = "Man";
                const data = [
                    {"id": "1", "character": "Ralph", "quote": "They taste like burning", "rank": 2},
                    {"id": "2", "character": "Numérobis", "quote": "3 mois? et avec combien de retard?", "rank": 1},
                    {"id": "3", "character": "Iron Man", "quote": "And I'm Iron Man", "rank": 4},
                    {"id": "4", "character": "Vador", "quote": "I'm your father", "rank": 3}
                ];
                const field1 = "quote";
                const field2 = "character";
                const resultat_attendu1 = [
                    {"id": "3", "character": "Iron Man", "quote": "And I'm Iron Man", "rank": 4},
                    {"id": "4", "character": "Vador", "quote": "I'm your father", "rank": 3}
                ];
                const resultat_attendu2 = [
                    {"id": "3", "character": "Iron Man", "quote": "And I'm Iron Man", "rank": 4}
                ];
                chai.assert.deepEqual(filter(filterSequence1, data, field1), resultat_attendu1);
                chai.assert.deepEqual(filter(filterSequence2, resultat_attendu1, field2), resultat_attendu2);
            });

        test("Filter by character + filter by quote",
            function () {
                const filterSequence1 = "a";
                const filterSequence2 = "I'm";
                const data = [
                    {"id": "1", "character": "Ralph", "quote": "They taste like burning", "rank": 2},
                    {"id": "2", "character": "Numérobis", "quote": "3 mois? et avec combien de retard?", "rank": 1},
                    {"id": "3", "character": "Iron Man", "quote": "And I'm Iron Man", "rank": 4},
                    {"id": "4", "character": "Vador", "quote": "I'm your father", "rank": 3}
                ];
                const field1 = "character";
                const field2 = "quote";
                const resultat_attendu1 = [
                    {"id": "1", "character": "Ralph", "quote": "They taste like burning", "rank": 2},
                    {"id": "3", "character": "Iron Man", "quote": "And I'm Iron Man", "rank": 4},
                    {"id": "4", "character": "Vador", "quote": "I'm your father", "rank": 3}
                ];
                const resultat_attendu2 = [
                    {"id": "3", "character": "Iron Man", "quote": "And I'm Iron Man", "rank": 4},
                    {"id": "4", "character": "Vador", "quote": "I'm your father", "rank": 3}
                ];
                chai.assert.deepEqual(filter(filterSequence1, data, field1), resultat_attendu1);
                chai.assert.deepEqual(filter(filterSequence2, resultat_attendu1, field2), resultat_attendu2);
            });

    });

//state.sorting.character = false (==caractère descendant) -> 2 sens
suite("Tests for function sort + filter",
    function () {

        test("Filter by character + sort character",
            function () {
                const filterSequence = "ro";
                const state = new State();
                const data = [
                    {"id": "1", "character": "Ralph", "quote": "They taste like burning", "rank": 2},
                    {"id": "2", "character": "Numérobis", "quote": "3 mois? et avec combien de retard?", "rank": 1},
                    {"id": "3", "character": "Iron Man", "quote": "And I'm Iron Man", "rank": 4},
                    {"id": "4", "character": "Vador", "quote": "I'm your father", "rank": 3}
                ];
                const field = "character";
                const resultat_attendu1 = [
                    {"id": "2", "character": "Numérobis", "quote": "3 mois? et avec combien de retard?", "rank": 1},
                    {"id": "3", "character": "Iron Man", "quote": "And I'm Iron Man", "rank": 4}
                ];
                const resultat_attendu2 = [
                    {"id": "3", "character": "Iron Man", "quote": "And I'm Iron Man", "rank": 4},
                    {"id": "2", "character": "Numérobis", "quote": "3 mois? et avec combien de retard?", "rank": 1}
                ];
                chai.assert.deepEqual(filter(filterSequence, data, field), resultat_attendu1);
                chai.assert.deepEqual(sort(state, resultat_attendu1, field), resultat_attendu2);
            });

        test("Sort character + filter by character",
            function () {
                const filterSequence = "ro";
                const state = new State();
                const data = [
                    {"id": "1", "character": "Ralph", "quote": "They taste like burning", "rank": 2},
                    {"id": "2", "character": "Numérobis", "quote": "3 mois? et avec combien de retard?", "rank": 1},
                    {"id": "3", "character": "Iron Man", "quote": "And I'm Iron Man", "rank": 4},
                    {"id": "4", "character": "Vador", "quote": "I'm your father", "rank": 3}
                ];
                const field = "character";
                const resultat_attendu1 = [
                    {"id": "3", "character": "Iron Man", "quote": "And I'm Iron Man", "rank": 4},
                    {"id": "2", "character": "Numérobis", "quote": "3 mois? et avec combien de retard?", "rank": 1},
                    {"id": "1", "character": "Ralph", "quote": "They taste like burning", "rank": 2},
                    {"id": "4", "character": "Vador", "quote": "I'm your father", "rank": 3},
                ];
                const resultat_attendu2 = [
                    {"id": "3", "character": "Iron Man", "quote": "And I'm Iron Man", "rank": 4},
                    {"id": "2", "character": "Numérobis", "quote": "3 mois? et avec combien de retard?", "rank": 1}
                ];
                chai.assert.deepEqual(sort(state, data, field), resultat_attendu1);
                chai.assert.deepEqual(filter(filterSequence, resultat_attendu1, field), resultat_attendu2);
            });

        test("Filter by quote + sort character",
            function () {
                const filterSequence = "I'm";
                const state = new State();
                const data = [
                    {"id": "1", "character": "Ralph", "quote": "They taste like burning", "rank": 2},
                    {"id": "2", "character": "Numérobis", "quote": "3 mois? et avec combien de retard?", "rank": 1},
                    {"id": "3", "character": "Iron Man", "quote": "And I'm Iron Man", "rank": 4},
                    {"id": "4", "character": "Vador", "quote": "I'm your father", "rank": 3}
                ];
                const field1 = "quote";
                const field2 = "character";
                const resultat_attendu1 = [
                    {"id": "3", "character": "Iron Man", "quote": "And I'm Iron Man", "rank": 4},
                    {"id": "4", "character": "Vador", "quote": "I'm your father", "rank": 3}
                ];
                const resultat_attendu2 = [
                    {"id": "3", "character": "Iron Man", "quote": "And I'm Iron Man", "rank": 4},
                    {"id": "4", "character": "Vador", "quote": "I'm your father", "rank": 3}
                ];
                chai.assert.deepEqual(filter(filterSequence, data, field1), resultat_attendu1);
                chai.assert.deepEqual(sort(state, resultat_attendu1, field2), resultat_attendu2);
            });

        test("Sort character + filter by quote",
            function () {
                const filterSequence = "I'm";
                const state = new State();
                const data = [
                    {"id": "1", "character": "Ralph", "quote": "They taste like burning", "rank": 2},
                    {"id": "2", "character": "Numérobis", "quote": "3 mois? et avec combien de retard?", "rank": 10},
                    {"id": "3", "character": "Iron Man", "quote": "And I'm Iron Man", "rank": 4},
                    {"id": "4", "character": "Vador", "quote": "I'm your father", "rank": 3}
                ];
                const field1 = "character";
                const field2 = "quote";
                const resultat_attendu1 = [
                    {"id": "3", "character": "Iron Man", "quote": "And I'm Iron Man", "rank": 4},
                    {"id": "2", "character": "Numérobis", "quote": "3 mois? et avec combien de retard?", "rank": 10},
                    {"id": "1", "character": "Ralph", "quote": "They taste like burning", "rank": 2},
                    {"id": "4", "character": "Vador", "quote": "I'm your father", "rank": 3},
                ];
                const resultat_attendu2 = [
                    {"id": "3", "character": "Iron Man", "quote": "And I'm Iron Man", "rank": 4},
                    {"id": "4", "character": "Vador", "quote": "I'm your father", "rank": 3}
                ];
                chai.assert.deepEqual(sort(state, data, field1), resultat_attendu1);
                chai.assert.deepEqual(filter(filterSequence, resultat_attendu1, field2), resultat_attendu2);
            });

        test("Filter by character + sort quote",
            function () {
                const filterSequence = "o";
                const state = new State();
                const data = [
                    {"id": "1", "character": "Ralph", "quote": "They taste like burning", "rank": 2},
                    {"id": "2", "character": "Numérobis", "quote": "3 mois? et avec combien de retard?", "rank": 10},
                    {"id": "3", "character": "Iron Man", "quote": "And I'm Iron Man", "rank": 4},
                    {"id": "4", "character": "Vador", "quote": "I'm your father", "rank": 3}
                ];
                const field1 = "character";
                const field2 = "quote";
                const resultat_attendu1 = [
                    {"id": "2", "character": "Numérobis", "quote": "3 mois? et avec combien de retard?", "rank": 10},
                    {"id": "3", "character": "Iron Man", "quote": "And I'm Iron Man", "rank": 4},
                    {"id": "4", "character": "Vador", "quote": "I'm your father", "rank": 3}
                ];
                const resultat_attendu2 = [
                    {"id": "2", "character": "Numérobis", "quote": "3 mois? et avec combien de retard?", "rank": 10},
                    {"id": "3", "character": "Iron Man", "quote": "And I'm Iron Man", "rank": 4},
                    {"id": "4", "character": "Vador", "quote": "I'm your father", "rank": 3}
                ];
                chai.assert.deepEqual(filter(filterSequence, data, field1), resultat_attendu1);
                chai.assert.deepEqual(sort(state, resultat_attendu1, field2), resultat_attendu2);
            });

        test("Sort quote + filter by character",
            function () {
                const filterSequence = "a";
                const state = new State();
                const data = [
                    {"id": "1", "character": "Ralph", "quote": "They taste like burning", "rank": 2},
                    {"id": "2", "character": "Numérobis", "quote": "3 mois? et avec combien de retard?", "rank": 10},
                    {"id": "3", "character": "Iron Man", "quote": "And I'm Iron Man", "rank": 4},
                    {"id": "4", "character": "Vador", "quote": "I'm your father", "rank": 3}
                ];
                const field1 = "quote";
                const field2 = "character";
                const resultat_attendu1 = [
                    {"id": "2", "character": "Numérobis", "quote": "3 mois? et avec combien de retard?", "rank": 10},
                    {"id": "3", "character": "Iron Man", "quote": "And I'm Iron Man", "rank": 4},
                    {"id": "4", "character": "Vador", "quote": "I'm your father", "rank": 3},
                    {"id": "1", "character": "Ralph", "quote": "They taste like burning", "rank": 2}
                ];
                const resultat_attendu2 = [
                    {"id": "3", "character": "Iron Man", "quote": "And I'm Iron Man", "rank": 4},
                    {"id": "4", "character": "Vador", "quote": "I'm your father", "rank": 3},
                    {"id": "1", "character": "Ralph", "quote": "They taste like burning", "rank": 2}
                ];
                chai.assert.deepEqual(sort(state, data, field1), resultat_attendu1);
                chai.assert.deepEqual(filter(filterSequence, resultat_attendu1, field2), resultat_attendu2);
            });

        test("Filter by quote + sort quote",
            function () {
                const filterSequence = "I'm";
                const state = new State();
                const data = [
                    {"id": "1", "character": "Ralph", "quote": "They taste like burning", "rank": 2},
                    {"id": "2", "character": "Numérobis", "quote": "3 mois? et avec combien de retard?", "rank": 10},
                    {"id": "3", "character": "Iron Man", "quote": "And I'm Iron Man", "rank": 4},
                    {"id": "4", "character": "Vador", "quote": "I'm your father", "rank": 3}
                ];
                const field = "quote";
                const resultat_attendu1 = [
                    {"id": "3", "character": "Iron Man", "quote": "And I'm Iron Man", "rank": 4},
                    {"id": "4", "character": "Vador", "quote": "I'm your father", "rank": 3}
                ];
                const resultat_attendu2 = [
                    {"id": "3", "character": "Iron Man", "quote": "And I'm Iron Man", "rank": 4},
                    {"id": "4", "character": "Vador", "quote": "I'm your father", "rank": 3}
                ];
                chai.assert.deepEqual(filter(filterSequence, data, field), resultat_attendu1);
                chai.assert.deepEqual(sort(state, resultat_attendu1, field), resultat_attendu2);
            });

        test("Sort quote + filter by quote",
            function () {
                const filterSequence = "I'm";
                const state = new State();
                const data = [
                    {"id": "1", "character": "Ralph", "quote": "They taste like burning", "rank": 2},
                    {"id": "2", "character": "Numérobis", "quote": "3 mois? et avec combien de retard?", "rank": 10},
                    {"id": "3", "character": "Iron Man", "quote": "And I'm Iron Man", "rank": 4},
                    {"id": "4", "character": "Vador", "quote": "I'm your father", "rank": 3}
                ];
                const field = "quote";
                const resultat_attendu1 = [
                    {"id": "2", "character": "Numérobis", "quote": "3 mois? et avec combien de retard?", "rank": 10},
                    {"id": "3", "character": "Iron Man", "quote": "And I'm Iron Man", "rank": 4},
                    {"id": "4", "character": "Vador", "quote": "I'm your father", "rank": 3},
                    {"id": "1", "character": "Ralph", "quote": "They taste like burning", "rank": 2}
                ];
                const resultat_attendu2 = [
                    {"id": "3", "character": "Iron Man", "quote": "And I'm Iron Man", "rank": 4},
                    {"id": "4", "character": "Vador", "quote": "I'm your father", "rank": 3}
                ];
                chai.assert.deepEqual(sort(state, data, field), resultat_attendu1);
                chai.assert.deepEqual(filter(filterSequence, resultat_attendu1, field), resultat_attendu2);
            });

        test("Filter by character + sort rank",
            function () {
                const filterSequence = "n";
                const state = new State();
                const data = [
                    {"id": "1", "character": "Ralph", "quote": "They taste like burning", "rank": 2},
                    {"id": "2", "character": "Numérobis", "quote": "3 mois? et avec combien de retard?", "rank": 1},
                    {"id": "3", "character": "Iron Man", "quote": "And I'm Iron Man", "rank": 4},
                    {"id": "4", "character": "Vador", "quote": "I'm your father", "rank": 3}
                ];
                const field1 = "character";
                const field2 = "rank";
                const resultat_attendu1 = [
                    {"id": "2", "character": "Numérobis", "quote": "3 mois? et avec combien de retard?", "rank": 1},
                    {"id": "3", "character": "Iron Man", "quote": "And I'm Iron Man", "rank": 4}
                ];
                const resultat_attendu2 = [
                    {"id": "2", "character": "Numérobis", "quote": "3 mois? et avec combien de retard?", "rank": 1},
                    {"id": "3", "character": "Iron Man", "quote": "And I'm Iron Man", "rank": 4}
                ];
                chai.assert.deepEqual(filter(filterSequence, data, field1), resultat_attendu1);
                chai.assert.deepEqual(sort(state, resultat_attendu1, field2), resultat_attendu2);
            });

        test("Sort rank + filter by character",
            function () {
                const filterSequence = "l";
                const state = new State();
                const data = [
                    {"id": "1", "character": "Ralph", "quote": "They taste like burning", "rank": 2},
                    {"id": "2", "character": "Numérobis", "quote": "3 mois? et avec combien de retard?", "rank": 1},
                    {"id": "3", "character": "Iron Man", "quote": "And I'm Iron Man", "rank": 4},
                    {"id": "4", "character": "Vador", "quote": "I'm your father", "rank": 3}
                ];
                const field1 = "rank";
                const field2 = "character";
                const resultat_attendu1 = [
                    {"id": "2", "character": "Numérobis", "quote": "3 mois? et avec combien de retard?", "rank": 1},
                    {"id": "1", "character": "Ralph", "quote": "They taste like burning", "rank": 2},
                    {"id": "4", "character": "Vador", "quote": "I'm your father", "rank": 3},
                    {"id": "3", "character": "Iron Man", "quote": "And I'm Iron Man", "rank": 4}
                ];
                const resultat_attendu2 = [
                    {"id": "1", "character": "Ralph", "quote": "They taste like burning", "rank": 2}
                ];
                chai.assert.deepEqual(sort(state, data, field1), resultat_attendu1);
                chai.assert.deepEqual(filter(filterSequence, resultat_attendu1, field2), resultat_attendu2);
            });

        test("Filter by quote + sort rank",
            function () {
                const filterSequence = "I'm";
                const state = new State();
                const data = [
                    {"id": "1", "character": "Ralph", "quote": "They taste like burning", "rank": 2},
                    {"id": "2", "character": "Numérobis", "quote": "3 mois? et avec combien de retard?", "rank": 1},
                    {"id": "3", "character": "Iron Man", "quote": "And I'm Iron Man", "rank": 4},
                    {"id": "4", "character": "Vador", "quote": "I'm your father", "rank": 3}
                ];
                const field1 = "quote";
                const field2 = "rank";
                const resultat_attendu1 = [
                    {"id": "3", "character": "Iron Man", "quote": "And I'm Iron Man", "rank": 4},
                    {"id": "4", "character": "Vador", "quote": "I'm your father", "rank": 3}
                ];
                const resultat_attendu2 = [
                    {"id": "4", "character": "Vador", "quote": "I'm your father", "rank": 3},
                    {"id": "3", "character": "Iron Man", "quote": "And I'm Iron Man", "rank": 4}
                ];
                chai.assert.deepEqual(filter(filterSequence, data, field1), resultat_attendu1);
                chai.assert.deepEqual(sort(state, resultat_attendu1, field2), resultat_attendu2);
            });

        test("Sort rank + filter by quote",
            function () {
                const filterSequence = "I'm";
                const state = new State();
                const data = [
                    {"id": "1", "character": "Ralph", "quote": "They taste like burning", "rank": 2},
                    {"id": "2", "character": "Numérobis", "quote": "3 mois? et avec combien de retard?", "rank": 1},
                    {"id": "3", "character": "Iron Man", "quote": "And I'm Iron Man", "rank": 4},
                    {"id": "4", "character": "Vador", "quote": "I'm your father", "rank": 3}
                ];
                const field1 = "quote";
                const field2 = "rank";
                const resultat_attendu1 = [
                    {"id": "2", "character": "Numérobis", "quote": "3 mois? et avec combien de retard?", "rank": 1},
                    {"id": "1", "character": "Ralph", "quote": "They taste like burning", "rank": 2},
                    {"id": "4", "character": "Vador", "quote": "I'm your father", "rank": 3},
                    {"id": "3", "character": "Iron Man", "quote": "And I'm Iron Man", "rank": 4}
                ];
                const resultat_attendu2 = [
                    {"id": "4", "character": "Vador", "quote": "I'm your father", "rank": 3},
                    {"id": "3", "character": "Iron Man", "quote": "And I'm Iron Man", "rank": 4}
                ];
                chai.assert.deepEqual(sort(state, data, field2), resultat_attendu1);
                chai.assert.deepEqual(filter(filterSequence, resultat_attendu1, field1), resultat_attendu2);
            });

    });


suite("Tests pour la fonction getScore",
    function () {

        test("getScore with wins",
            function () {
                const state = new State();
                const scores = {
                    "608d8209251604170f2988f7": {_id: "608d84ebc54bf07bf3982c5e", wins: 1, looses: 0},
                    "608d8209251604170f29890a": {_id: "608d8346c54bf07bf3982b45", wins: 0, looses: 1},
                    "608d8209251604170f2988a8": {_id: "608d84ebc54bf07bf3982c67", wins: 3, looses: 2},
                    "608d8209251604170f2988j2": {_id: "608d84ebc54bf07bf3982c91", wins: 2, looses: 8},
                    "608d8209251672170f2988a8": {_id: "608d84ebc81bf07bf3982c67", wins: 7, looses: 4}
                };
                const resultat_attendu = 13;
                chai.assert.deepEqual(getScore(state, scores), resultat_attendu);
            });

        test("getScore without wins",
            function () {
                const state = new State();
                const scores = {
                    "608d8209251604170f2988f7": {_id: "608d84ebc54bf07bf3982c5e", wins: 0, looses: 0},
                    "608d8209251604170f29890a": {_id: "608d8346c54bf07bf3982b45", wins: 0, looses: 1},
                    "608d8209251604170f2988a8": {_id: "608d84ebc54bf07bf3982c67", wins: 0, looses: 2},
                    "608d8209251604170f2988j2": {_id: "608d84ebc54bf07bf3982c91", wins: 0, looses: 8},
                    "608d8209251672170f2988a8": {_id: "608d84ebc81bf07bf3982c67", wins: 0, looses: 4}
                };
                const resultat_attendu = 0;
                chai.assert.deepEqual(getScore(state, scores), resultat_attendu);
            });

    });


suite("Tests pour la fonction makeRanking",
    function () {

        test("makeRanking total wins",
            function () {
                const state = new State();
                state.data = [
                    {
                        "quote": "J'ai connu une polonaise qui en prenait au petit déjeuner... \nFaut quand même admettre que c'est plutôt une boisson d'homme.\n",
                        "character": "Fernand Naudin",
                        "scores": {
                            "6059c14d617ca98251240091": {
                                "wins": 5,
                                "looses": 2
                            },
                            "6058567c999f8d1643f84c14": {
                                "wins": 0,
                                "looses": 3
                            }
                        },
                        "id": "6058567c999f8d1643f84c08"
                    },
                    {
                        "quote": "Et nous on a un Hulk.",
                        "character": "Loki",
                        "scores": {
                            "6059c14d617ca98251240091": {
                                "wins": 8,
                                "looses": 3
                            },
                            "6058567c999f8d1643f84c14": {
                                "wins": 2,
                                "looses": 5
                            }
                        },
                        "id": "6058567c999f8d1643f84c92"
                    },
                    {
                        "quote": "I'm your father",
                        "character": "Vador",
                        "scores": {
                            "6059c14d617ca98251240091": {
                                "wins": 2,
                                "looses": 9
                            },
                            "6058567c999f8d1643f84c14": {
                                "wins": 0,
                                "looses": 1
                            }
                        },
                        "id": "6058567c999f8d1643f84c88"
                    },
                    {
                        "quote": "And I'm... Iron man",
                        "character": "Iron Man",
                        "scores": {
                            "6059c14d617ca98251240091": {
                                "wins": 10,
                                "looses": 0
                            },
                            "6058567c999f8d1643f84c14": {
                                "wins": 7,
                                "looses": 3
                            }
                        },
                        "id": "6058567c999f8d1643f84c61"
                    }
                ];
                const resultat_attendu = [
                    {
                        "quote": "J'ai connu une polonaise qui en prenait au petit déjeuner... \nFaut quand même admettre que c'est plutôt une boisson d'homme.\n",
                        "character": "Fernand Naudin",
                        "scores": {
                            "6059c14d617ca98251240091": {
                                "wins": 5,
                                "looses": 2
                            },
                            "6058567c999f8d1643f84c14": {
                                "wins": 0,
                                "looses": 3
                            }
                        },
                        "id": "6058567c999f8d1643f84c08",
                        "rank": 3
                    },
                    {
                        "quote": "Et nous on a un Hulk.",
                        "character": "Loki",
                        "scores": {
                            "6059c14d617ca98251240091": {
                                "wins": 8,
                                "looses": 3
                            },
                            "6058567c999f8d1643f84c14": {
                                "wins": 2,
                                "looses": 5
                            }
                        },
                        "id": "6058567c999f8d1643f84c92",
                        "rank": 2
                    },
                    {
                        "quote": "I'm your father",
                        "character": "Vador",
                        "scores": {
                            "6059c14d617ca98251240091": {
                                "wins": 2,
                                "looses": 9
                            },
                            "6058567c999f8d1643f84c14": {
                                "wins": 0,
                                "looses": 1
                            }
                        },
                        "id": "6058567c999f8d1643f84c88",
                        "rank": 4
                    },
                    {
                        "quote": "And I'm... Iron man",
                        "character": "Iron Man",
                        "scores": {
                            "6059c14d617ca98251240091": {
                                "wins": 10,
                                "looses": 0
                            },
                            "6058567c999f8d1643f84c14": {
                                "wins": 7,
                                "looses": 3
                            }
                        },
                        "id": "6058567c999f8d1643f84c61",
                        "rank": 1
                    }
                ]
                makeRanking(state);
                chai.assert.deepEqual(state.data, resultat_attendu);
            });

    });