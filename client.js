/*
 * Constantes
 */

const API_KEY = "7adbe2dc-6070-409e-92fa-d6dbf2a4828f";
const SERVER_URL = "https://lifap5.univ-lyon1.fr/";

const SUB_MENUS = ["vote", "list", "add"]; // Tableau de données constant représentants les menus

/**
 * Rempli le tableau de citations avec les données contenues dans l'état courant
 *
 * @param state État courant
 */
function fillTable(state) {
    const tableBody = document.getElementById("list-table-body");
    tableBody.innerHTML = "";

    Object.values(state.printedData)
        .map(quote => {
            const row = tableBody.insertRow();

            const rankCell = row.insertCell(0);
            rankCell.innerHTML = quote.rank.toString();

            const characterCell = row.insertCell(1);
            characterCell.innerHTML = quote.character;

            const quoteCell = row.insertCell(2);
            quoteCell.innerHTML = quote.quote;

            row.onclick = () => fetchAndPrintDetails(state, quote.id);
            row.style.cursor = "pointer";
            row.onmouseover = () => row.classList.add("is-selected");
            row.onmouseleave = () => row.classList.remove("is-selected");
        }, ""); // On aurait également pu mettre un for...each (reduce ne marche pas à cause du onclick)
}

/**
 * Tri les données passées en paramètre en fonction d'un certain champ passé en paramètre
 *
 * @param {State} state État courant
 * @param {Array} data Données à trier
 * @param {String} field Nom du champ à trier (=colonne)
 * @returns {Array} Données triées
 */
function sort(state, data, field) {
    return data
        .sort((quote1, quote2) => {
            const field1 = quote1[field];
            const field2 = quote2[field];

            if (field1 < field2) {
                return state.sorting[field] ? -1 : 1;
            } else if (field1 > field2) {
                return state.sorting[field] ? 1 : -1;
            } else {
                return 0;
            }
        });
}

/**
 * Tri les citations contenues dans l'état courant et met à jour le tableau avec les données triées
 *
 * @param {State} state État courant
 * @param {Boolean} inverse Défini si on doit inverser le tri actuel (true : inversion du tri, false : pas d'inversion de tri)
 * @param {String} field Nom du champ à trier (=colonne)
 */
function sortField(state, inverse = false, field = state.lastSort) {
    if (inverse) {
        state.sorting[field] = !state.sorting[field]; // Inversion du tri
        updateSorting(state);
    }

    state.printedData = sort(state, Object.values(state.printedData), field);

    state.lastSort = field; // On actualise le dernier tri

    fillTable(state);
}

/**
 * Filtre les données passées en paramètre en ne gardant que celles contenant une certaine séquence de caractères
 *
 * @param {String} filterSequence Chaîne de caractère servant de filtre, insensible à la casse.
 * @param {Array} data Données à filtrer
 * @param {String} field Nom du champ à trier (=colonne)
 * @returns {*} Données filtrées
 */
function filter(filterSequence, data, field) {
    return data.filter(quote => {
        if (quote[field] !== undefined) {
            const parsedField = quote[field].toString().toLowerCase();

            return parsedField.indexOf(filterSequence.toLowerCase()) > -1;
        } else {
            return false;
        }
    });
}

/**
 * Filtre les colonnes (insensible à la casse) en ne gardant que les citations contenant une certaine chaîne de caractères
 * et met à jour le tableau avec les nouvelles données triées
 *
 * @param {State} state État courant
 */
function filterField(state) {
    // Premier filtre
    const inputCharacter = document.getElementById("in-filter-" + "character");
    const filterSequenceCharacter = inputCharacter.value.toLowerCase();

    state.printedData = filter(
        filterSequenceCharacter,
        Object.values(state.data),
        "character");

    // Deuxième filtre
    const inputQuote = document.getElementById("in-filter-" + "quote");
    const filterSequenceQuote = inputQuote.value.toLowerCase();

    state.printedData = filter(
        filterSequenceQuote,
        Object.values(state.printedData),
        "quote");

    sortField(state); // On tri correctement

    fillTable(state); // On affiche les données
}

/**
 * Builder de requêtes
 *
 * @param request Requête à envoyer au serveur
 * @param apiKey Clé d'API
 * @returns {Promise<any | {error: any}>} Résultat de la requête sous forme d'une promesse
 */
function fetchBuilder(request, apiKey = API_KEY) {
    return fetch(SERVER_URL + request,
        {headers: {"x-api-key": apiKey, "Access-Control-Allow-Origin": "*"}})
        .then(response => response.json())
        .then(jsonData => {
            if (jsonData.status && Number(jsonData.status) !== 200) {
                return {error: jsonData.message};
            } else {
                return jsonData;
            }
        })
        .catch(reason => ({error: reason}));
}

/**
 * Met au besoin à jour l'état courant lors d'un click sur un nav.
 * En cas de mise à jour, déclenche une mise à jour de la page.
 *
 * @param {String} nav Nom du nav qui a été cliqué
 * @param {State} state État courant
 */
function navClick(nav, state) {
    if (state.state !== nav) {
        state.state = nav;

        updatePage(state);
    }
}

/**
 * Déclenche l'affichage de la boîte de dialogue du nom de l'utilisateur.
 *
 * @param {State} state État courant
 */
function closeLoginModal(state) {
    state.modals.login = false;

    if (!state.isLogged) {
        document.getElementById("elt-affichage-login").innerHTML = "";
        document.getElementById("in-password").value = "";
    }

    updatePage(state);
}

/**
 * Déclenche la fermeture de la boîte de dialogue du nom de l'utilisateur.
 *
 * @param {State} state État courant
 */
function openLoginModal(state) {
    state.modals.login = true;

    updatePage(state);
}

/**
 * Affiche la notification de login
 */
function printLoginNotification() {
    const notification = document.getElementById("ntf-login");
    const notificationElement = document.getElementById("elt-login");
    notificationElement.innerHTML = "Vous êtes désormais connecté !";

    notification.style.display = "flex";
}

/**
 * Connecte l'utilisateur et met à jour la boîte de dialogue de connexion
 *
 * @param {State} state État courant
 */
function login(state) {
    const password = document.getElementById("in-password");

    fetchBuilder("whoami", password.value)
        .then(data => {
            const element = document.getElementById("elt-affichage-login");

            if (data.error === undefined) {
                state.isLogged = true;

                password.value = "";

                element.innerHTML = `Bonjour ${data.login}`;
                printLoginNotification();

                closeLoginModal(state);
            } else {
                element.innerHTML = "Mauvais utilisateur";
            }
        });
}

/**
 * Déconnecte l'utilisateur et met à jour la boîte de dialogue de connexion
 *
 * @param {State} state État courant
 */
function logout(state) {
    state.isLogged = false;

    const notification = document.getElementById("ntf-logout");
    const notificationElement = document.getElementById("elt-logout");
    notificationElement.innerHTML = "Vous êtes désormais déconnecté !";

    notification.style.display = "flex";

    closeLoginModal(state);
}

/**
 * Affiche le modal associé aux détails d'une citation
 *
 * @param {State} state État courant
 */
function openDetailsModal(state) {
    state.modals.details = true;

    updatePage(state);
}

/**
 * Cache le modal associé aux détails d'une citation
 *
 * @param {State} state État courant
 */
function closeDetailsModal(state) {
    state.modals.details = false;

    updatePage(state);
}

/**
 * Rempli le tableau de scores des détails d'une citation
 *
 * @param {Array} data Scores de la citation
 */
function fillDetailsTable(data) {
    const tableBody = document.getElementById("score-table-body");

    tableBody.innerHTML = data.reduce((acc, value) => {
            if (value !== undefined) {
                return acc +
                    `<tr>
                        <td>${value["quote"]}</td>
                        <td>${value["wins"]}</td>
                        <td>${value["loses"]}</td>
                    </tr>`;
            } else {
                return acc;
            }
        }
        , "");
}

/**
 * Attribue un label à chacune des données d'une citation
 * @param {Object} quote Citation à labéliser
 * @return {Object} Les détails de la citation labélisés
 */
function labelCitation(quote) {
    return {
        "Citation": quote.quote,
        "Personnage": quote.character,
        "Direction du personnage": quote.characterDirection,
        "Origine": quote.origin,
        "Ajoutée par": quote.addedBy
    };
}

/**
 * Affiche les détails d'une citation
 *
 * @param {Object} data Citation sous la forme d'un tableau associatif
 */
function printDetails(data) {
    const element = document.getElementById(
        "elt-affichage-details"
    );

    element.innerHTML = Object.keys(data)
        .reduce((acc, key) =>
            acc + `<p> ${key} : ${data[key]} </p>`, ""
        );
}

/**
 * Récupère et affiche les détails de la citation dont l'identifiant est passé en paramètre
 *
 * @param {State} state État courant
 * @param {int} id Identifiant de la citation à afficher
 */
function fetchAndPrintDetails(state, id) {
    fetchBuilder("citations/" + id)
        .then(data => {
                if (data.error === undefined) {
                    const newData = labelCitation(data);

                    printDetails(newData);

                    data = Object.keys(data.scores)
                        .map(id => ({
                            "quote": state.data[id]["quote"],
                            "wins": data.scores[id].wins,
                            "loses": data.scores[id].looses
                        }))

                    fillDetailsTable(data);

                    openDetailsModal(state);
                }
            }
        );
}

/* === Classement ===

   ================== */

/**
 * Calcule le score d'une citation par nombre de victoires absolues
 *
 * @param {State} state État courant
 * @param {Array} scores Tableau des scores de la citation
 * @returns {int} Score de la citation
 */
function getScore(state, scores) {
    return Object.values(scores)
        .reduce((acc, image) => acc + parseInt(image.wins), 0);
}

/**
 * Détermine le rang de chacune des citations en fonction de leur score
 *
 * @param {State} state État courant
 */
function makeRanking(state) {
    let rank = 1;

    Object.values(state.data)
        .sort((quote1, quote2) => {
            const score1 = getScore(state, quote1.scores);
            const score2 = getScore(state, quote2.scores);

            return score2 - score1;
        })
        .map(quote => {
            quote.rank = rank;
            rank++;
        }); // On aurait également pu mettre un for each
}

/**
 * Récupère l'ensemble des citations sur le serveur, les stocke dans l'état courant et les affiche
 *
 * @param {State} state État courant
 */
function fetchCitations(state) {
    fetchBuilder("citations")
        .then(data => {
            if (data.error === undefined) {
                data
                    .filter(quote => quote.scores !== undefined)
                    .map(quote => {
                        state.data[quote._id] = {
                            "id": quote._id,
                            "character": quote.character,
                            "quote": quote.quote,
                            "scores": quote.scores
                        }; // Recopie des données dans le cache (On aurait également pu mettre un for each)
                    });

                state.printedData = state.data; // Initialisation des données affichées

                makeRanking(state); // Établissement du classement
                sortField(state); // Tri des données
                fillTable(state); // Remplissage du tableau
            }
        });
}

/*============
    REGISTER
 =============*/

/**
 * Enregistre les actions à effecteur lorsque l'on clique
 * sur un des tabs.
 *
 * @param {State} state État courant
 */
function registerNavClick(state) {
    SUB_MENUS.map(subMenu => {
            document.getElementById("nav-" + subMenu).onclick =
                () => navClick(subMenu, state);

            return subMenu;
        }
    ); // On aurait également pu mettre un for each comme on ne modifie pas subMenu
}

/**
 * Enregistre les actions à effectuer lors d'un click sur les boutons
 * d'ouverture/fermeture du modal associé à la connexion.
 *
 * @param {State} state État courant
 */
function registerLoginModalClick(state) {
    document.getElementById("btn-close-login-modal1").onclick = () =>
        closeLoginModal(state);
    document.getElementById("btn-close-login-modal2").onclick = () =>
        closeLoginModal(state);
    document.getElementById("btn-open-login-modal").onclick = () =>
        openLoginModal(state);

    document.getElementById("btn-login").onclick = () => login(state);
    document.getElementById("btn-logout").onclick = () => logout(state);
}

/**
 * Enregistre les actions à effectuer lors d'un click sur les boutons
 * de fermeture du modal associé aux détails d'une citation.
 *
 * @param {State} state État courant
 */
function registerDetailsModalClick(state) {
    document.getElementById("btn-close-details-modal1").onclick = () =>
        closeDetailsModal(state);
    document.getElementById("btn-close-details-modal2").onclick = () =>
        closeDetailsModal(state);
}

/**
 * Enregistre les actions à effectuer lors d'un click sur les en têtes des colonnes
 * marquées comme triables.
 *
 * @param {State} state État courant
 */
function registerSortClick(state) {
    const sortFactory = function (field) {
        document.getElementById(`th-${field}`).onclick = () =>
            sortField(state, true, field);
    };

    sortFactory("rank");
    sortFactory("character");
    sortFactory("quote");
}

/**
 * Enregistre les actions à effectuer lorsque quelque chose a été écrit
 * dans les champ de filtre
 *
 * @param {State} state État courant
 */
function registerFilterKeyUp(state) {
    const filterFactory = function (field) {
        document.getElementById(`in-filter-${field}`).onkeyup = () =>
            filterField(state);
    };

    filterFactory("character");
    filterFactory("quote");
}

/*============
    UPDATE
==============*/

/**
 * Met à jour le menu de navigation en fonction de la page affichée
 *
 * @param {State} state État courant
 */
function updateNav(state) {
    // On active le sous menu correspond à l'état courant
    document.getElementById("nav-" + state.state)
        .classList.add("is-active");
    document.getElementById("div-" + state.state)
        .style.display = "flex";

    // On désactive les autres sous menus
    SUB_MENUS
        .filter(other => other !== state.state)
        .map(other => {
            document.getElementById("nav-" + other)
                .classList.remove("is-active");
            document.getElementById("div-" + other)
                .style.display = "none";
        }); // On aurait également pu mettre un forEach ici comme on ne modifie pas le tableau de sous menus
}

/**
 * Met à jour l'affichage relatif à la connexion lorsque l'utilisateur est connecté
 */
function updateDataOnLogin() {
    document.getElementById("btn-open-login-modal")
        .innerHTML = "Déconnexion";

    document.getElementById("div-login")
        .style.display = "none";

    document.getElementById("btn-login")
        .style.display = "none";
    document.getElementById("btn-logout")
        .style.display = "flex";
}

/**
 * Met à jour l'affichage relatif à la connexion lorsque l'utilisateur n'est pas connecté
 */
function updateDataOnLogout() {
    document.getElementById("btn-open-login-modal")
        .innerHTML = "Connexion";

    document.getElementById("div-login")
        .style.display = "block";

    document.getElementById("btn-login")
        .style.display = "flex";
    document.getElementById("btn-logout")
        .style.display = "none";
}

/**
 * Affiche ou masque le modal du login en fonction de l'état courant
 *
 * @param {State} state État courant
 */
function updateLoginModal(state) {
    const modalClasses = document.getElementById("mdl-login").classList;

    if (state.modals.login) {
        modalClasses.add("is-active");
    } else {
        modalClasses.remove("is-active");
    }

    if (state.isLogged) {
        updateDataOnLogin();
    } else {
        updateDataOnLogout();
    }
}

/**
 * Affiche ou masque le modal associé aux détails d'une citation en fonction de l'état courant
 *
 * @param {State} state État courant
 */
function updateDetailsModal(state) {
    const modalClasses = document.getElementById("mdl-details").classList;

    if (state.modals.details) {
        modalClasses.add("is-active");
    } else {
        modalClasses.remove("is-active");
    }
}

/**
 * Met à jour les bouttons de tri (tri ascendant / descendant)
 *
 * @param {State} state État courant
 */
function updateSorting(state) {
    Object.entries(state.sorting)
        .map(([sort, value]) => {
            const icon = document.getElementById("icon-sort-" + sort);

            icon.classList.remove("fas", "fa-sort-up", "fa-sort-down");
            icon.classList.add("fas");

            if (value) {
                icon.classList.add("fa-sort-up");
            } else {
                icon.classList.add("fa-sort-down");
            }
        }); // For...each obligatoire ici car on agit sur la page et qu'on ne modifie pas le tableau d'origine
}

/**
 * Met à jour la page (contenu et événements) en fonction d'un nouvel état
 *
 * @param {State} state État courant
 */
function updatePage(state) {
    updateNav(state);
    updateLoginModal(state);
    updateDetailsModal(state);
    updateSorting(state);

    registerNavClick(state);
    registerLoginModalClick(state);
    registerDetailsModalClick(state);
    registerSortClick(state);
    registerFilterKeyUp(state);
}

/**
 * Appelée après le chargement de la page
 *
 * Met en place la mécanique de gestion des événements
 * en lançant la mise à jour de la page à partir d'un état initial
 */
function init() {
    const initialState = new State();
    updatePage(initialState);

    fetchCitations(initialState);
}

/**
 * Constructeur de State
 *
 * this.state : nom de l'état courant
 * this.modals : tableau associatif de booléans. La clé correspondant à un modal
 * et la valeur associée est un booléan définissant si oui ou non on affiche le modal sur la page
 * this.sorting : tableau associatif de booléans. La clé correspond à un champ
 * et la valeur associée est un booléan définissant l'ordre de tri pour le champ en question
 * (true : tri ascendant, false tri déscendant)
 * this.lastSort : dernier champ trié
 * this.isLogged : booléan définissant si oui ou non l'utilisateur est connecté
 * this.data : tableau de citations
 * this.printedData : tableau de citations couramment affichées
 *
 * @constructor
 */
function State() {
    this.state = "vote";
    this.modals = {
        "login": false,
        "details": false
    };
    this.sorting = {
        "rank": true,
        "character": true,
        "quote": true
    };
    this.lastSort = "rank";
    this.isLogged = false;
    this.data = [];
    this.printedData = [];
}

// Appel de la fonction init() après le chargement de la page
document.addEventListener("DOMContentLoaded", () => {
    init();
});

// Bulma (fermeture des modals) (Code trouvé sur leur documentation)
document.addEventListener('DOMContentLoaded', () => {
    (document.querySelectorAll('.notification .delete') || [])
        .forEach(($delete) => {
            const $notification = $delete.parentNode;

            $delete.addEventListener('click', () => {
                $notification.style.display = "none";
            });
        });
});